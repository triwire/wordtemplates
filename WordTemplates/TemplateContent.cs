using System.Collections.Generic;

namespace WordTemplates
{
    public class TemplateContent

    { 
        public Dictionary<string, TemplateValue> Values { get; } = new Dictionary<string, TemplateValue>();
        public Dictionary<string, List<TemplateContent>> Items { get; } = new Dictionary<string, List<TemplateContent>>();

    }
}