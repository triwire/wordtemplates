using System;
using WordTemplates.ConfigBuilders;

namespace WordTemplates
{
    public class TemplateConfig<TContext,TItem>
    {
        private readonly Action<TemplateContent, TContext, TItem> _action;
        public Template Template { get; }

        public TemplateContent GetContent(TContext context,TItem item)
        {
            var content = new TemplateContent();
            _action.Invoke(content,context,item);

            return content;
        }

        public static TemplateConfig<TContext,TItem> Build(Action<TemplateConfigBuilder<TContext, TItem>> builder_func)

        {
            var template = new Template();
            var builder = new TemplateConfigBuilder<TContext, TItem>(template);

            builder_func?.Invoke(builder);

            var action = builder.GetItemsAction();

            return new TemplateConfig<TContext, TItem>(template,action);
        }



        public TemplateConfig(Template template, Action<TemplateContent, TContext, TItem> action)
        {
            Template = template;
            _action = action;
        }
    }
}