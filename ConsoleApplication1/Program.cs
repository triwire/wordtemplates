﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using WordTemplates;
using WordTemplates.Core;
using Template = WordTemplates.Template;

namespace ConsoleApplication1
{
    class Program
    {


        private static void TestTemplateConfig()
        {

            var tt = TemplateConfig<string, string[]>.Build(b => b
                .AddValue("first", x => x.Item.FirstOrDefault())
                .AddGroup("firstLover",x=>x.Item.Select(s=>s.FirstOrDefault()).Where(s=>s!= default(char)).ToArray(),r=>r
                    .AddValue("length",c=>c.Item.Length.ToString())
                    .AddCollection("chars",x=>x.Item,r2=>r2.AddValue("c",x=>x.Item.ToString()))
                    .AddGroup("",x=>string.Concat(x.Item),r2=>r2.AddValue("string",x=>x.Item)))
                
            );

            var con = tt.GetContent("hello",new[] { "","SDSds","fsdfs"});

        }


        private static Stream Open()
        {
            return File.Open("123.docx", FileMode.Open, FileAccess.ReadWrite);
        }
        private static Stream OpenCopy()
        {
            var stream = new MemoryStream();
            using (var file = Open())
            {
                file.CopyTo(stream);
            }
            return stream;
        }


        static void Main()
        {


            TestTemplateConfig();

            using (var stream = OpenCopy())
            {

                DocumentConverter.ToHtml(stream).Save(File.Create("1.html"));

            }

            using (var stream = OpenCopy())
            {
                DocumentConverter.ToHtmlTemplate(stream).Save(File.Create("template.html"));
            }

            using (var file = OpenCopy())
            {


                var t = DocumentProcessor.GetTemplate(file);

                var data = t.GenerateTestContent();



                Print(null, t);

                DocumentProcessor.ProcessContent(file, data);
                using (var f = File.Create("123result.docx"))
                {
                    file.Seek(0, SeekOrigin.Begin);    
                    file.CopyTo(f);

                }
                


            }

            using (var stream = OpenCopy())
            {
                DocumentConverter.ToHtml(stream).Save(File.Create("2.html"));
            }

        }

        private static void Print(string x, Template t)
        {


            Console.WriteLine($"{x}tags: {t.Tags}");

            Console.WriteLine($"{x}values({t.Values.Count}):");
            foreach (var k in t.Values)
            {
                Console.WriteLine($" {x}{k.Key}: {k.Value}");

            }
            Console.WriteLine($"{x}templates({t.Items.Count}):");
            foreach (var tt in t.Items)
            {
                Console.WriteLine($"{x} {tt.Key}");
                Print(x + "  ", tt.Value);
            }

        }
    }
}
